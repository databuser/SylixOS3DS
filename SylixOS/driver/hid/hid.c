/*
 * hid.c
 *
 *  Created on: Nov 8, 2020
 *      Author: Administrator
 */
#define  __SYLIXOS_KERNEL
#include <SylixOS.h>

#include "types.h"
#include "mem_map.h"
#include "arm11/hardware/interrupt.h"

#define HID_REGS_BASE     (IO_MEM_ARM9_ARM11 + 0x46000)
#define REG_HID_PAD       *((vu16*)(HID_REGS_BASE + 0x0))
#define REG_HID_PADCNT    *((vu16*)(HID_REGS_BASE + 0x2))


static irqreturn_t hidIrqHandler(void)
{

    return  (LW_IRQ_HANDLED);
}


void hidInit(void)
{
    REG_HID_PADCNT = (1 << 1) | (1 << 14);

    API_InterVectorConnect(IRQ_HID_PADCNT,
                           (PINT_SVR_ROUTINE)hidIrqHandler,
                           LW_NULL,
                           "hid");
    API_InterVectorEnable(IRQ_HID_PADCNT);
}
