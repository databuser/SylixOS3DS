/*********************************************************************************************************
**
**                                    中国软件开源组织
**
**                                   嵌入式实时操作系统
**
**                                SylixOS(TM)  LW : long wing
**
**                               Copyright All Rights Reserved
**
**--------------文件信息--------------------------------------------------------------------------------
**
** 文   件   名: console_sio.c
**
** 创   建   人: Jiao.JinXing (焦进星)
**
** 文件创建日期: 2016 年 07 月 21 日
**
** 描        述: x86 console SIO 驱动.
*********************************************************************************************************/
#define  __SYLIXOS_KERNEL
#define  __SYLIXOS_STDIO
#include "SylixOS.h"
#include "con_sio.h"
#include "console.h"
#include "fmt.h"

/*********************************************************************************************************
  类型定义
*********************************************************************************************************/
typedef struct {                                                        /*  console SIO 通道控制块      */
    SIO_CHAN            SIOCHAN_sioChan;                                /*  SIO 通道                    */

    INT               (*SIOCHAN_pcbGetTxChar)();                        /*  中断回调函数                */
    INT               (*SIOCHAN_pcbPutRcvChar)();

    PVOID               SIOCHAN_pvGetTxArg;                             /*  回调函数参数                */
    PVOID               SIOCHAN_pvPutRcvArg;

    INT                 SIOCHAN_iBaud;                                  /*  波特率                      */
    INT                 SIOCHAN_iHwOption;                              /*  硬件选项                    */
} __CONSOLE_SIOCHAN;
typedef __CONSOLE_SIOCHAN    *__PCONSOLE_SIOCHAN;
/*********************************************************************************************************
  全局变量
*********************************************************************************************************/
static __CONSOLE_SIOCHAN      _G_consoleSioChannel;                     /*  console SIO 通道控制块      */
extern PrintConsole top_con;
/*********************************************************************************************************
** 函数名称: __consoleSioCbInstall
** 功能描述: SIO 通道安装回调函数
** 输　入  : pSioChan                  SIO 通道
**           iCallbackType             回调类型
**           callbackRoute             回调函数
**           pvCallbackArg             回调参数
** 输　出  : ERROR_CODE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __consoleSioCbInstall (SIO_CHAN        *pSioChan,
                                   INT              iCallbackType,
                                   VX_SIO_CALLBACK  callbackRoute,
                                   PVOID            pvCallbackArg)
{
    __PCONSOLE_SIOCHAN  pConsoleSioChan = (__PCONSOLE_SIOCHAN)pSioChan;

    switch (iCallbackType) {

    case SIO_CALLBACK_GET_TX_CHAR:                                      /*  发送回电函数                */
        pConsoleSioChan->SIOCHAN_pcbGetTxChar = callbackRoute;
        pConsoleSioChan->SIOCHAN_pvGetTxArg   = pvCallbackArg;
        return  (ERROR_NONE);

    case SIO_CALLBACK_PUT_RCV_CHAR:                                     /*  接收回电函数                */
        pConsoleSioChan->SIOCHAN_pcbPutRcvChar = callbackRoute;
        pConsoleSioChan->SIOCHAN_pvPutRcvArg   = pvCallbackArg;
        return  (ERROR_NONE);

    default:
        _ErrorHandle(ENOSYS);
        return  (PX_ERROR);
    }
}
/*********************************************************************************************************
** 函数名称: __consoleSioPollRxChar
** 功能描述: SIO 通道轮询接收
** 输　入  : pSioChan                  SIO 通道
**           pcInChar                  接收的字节
** 输　出  : ERROR_CODE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __consoleSioPollRxChar (SIO_CHAN  *pSioChan, PCHAR  pcInChar)
{
    return  (PX_ERROR);
}
/*********************************************************************************************************
** 函数名称: __consoleSioPollTxChar
** 功能描述: SIO 通道轮询发送
** 输　入  : pSioChan                  SIO 通道
**           cOutChar                  发送的字节
** 输　出  : ERROR_CODE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __consoleSioPollTxChar (SIO_CHAN  *pSioChan, CHAR  cOutChar)
{
    return  (PX_ERROR);
}
/*********************************************************************************************************
** 函数名称: __consoleSioStartup
** 功能描述: SIO 通道发送(没有使用中断)
** 输　入  : pSioChan                  SIO 通道
** 输　出  : ERROR_CODE
** 全局变量:
** 调用模块:
**
** 注    意: 这个函数有可能在非发送中断的中断里调用!
*********************************************************************************************************/
static INT  __consoleSioStartup (SIO_CHAN  *pSioChan)
{
    __PCONSOLE_SIOCHAN  pConsoleSioChan = (__PCONSOLE_SIOCHAN)pSioChan;

    CHAR cChar;
    CHAR cSendMsg[2] = {0,0};

    consoleSelect(&top_con);

    while (pConsoleSioChan->SIOCHAN_pcbGetTxChar(pConsoleSioChan->SIOCHAN_pvGetTxArg,
                                                 &cChar) == ERROR_NONE) {
        cSendMsg[0] = cChar;
        ee_printf(cSendMsg);
    }

    return  (ERROR_NONE);
}
#if 0
/*********************************************************************************************************
** 函数名称: __consoleSioKbdPutRcvChar
** 功能描述: keyboard 传入一个接收到的字符
** 输　入  : pConsoleSioChan          SIO 通道
**           cChar                    字符
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static VOID  __consoleSioKbdPutRcvChar (__PCONSOLE_SIOCHAN    pConsoleSioChan, CHAR  cChar)
{
    if (pConsoleSioChan->SIOCHAN_pcbPutRcvChar) {
        pConsoleSioChan->SIOCHAN_pcbPutRcvChar(pConsoleSioChan->SIOCHAN_pvPutRcvArg, cChar);
    }
}
#endif
/*********************************************************************************************************
** 函数名称: __consoleSioIoctl
** 功能描述: SIO 通道控制
** 输　入  : pSioChan                  SIO 通道
**           iCmd                      命令
**           lArg                      参数
** 输　出  : ERROR_CODE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  __consoleSioIoctl (SIO_CHAN  *pSioChan, INT  iCmd, LONG  lArg)
{
    __PCONSOLE_SIOCHAN  pConsoleSioChan = (__PCONSOLE_SIOCHAN)pSioChan;

    switch (iCmd) {

    case SIO_OPEN:                                                      /*  打开 SIO                    */
        pConsoleSioChan->SIOCHAN_iBaud     = SIO_BAUD_115200;           /*  初始化波特率                */
        pConsoleSioChan->SIOCHAN_iHwOption = CREAD | CS8;               /*  初始化硬件状态              */



#if 0
        if (pcKeyboardInit((VOIDFUNCPTR)__consoleSioKbdPutRcvChar,
                           pConsoleSioChan) != ERROR_NONE) {            /*  PS2 键盘                    */
            _ErrorHandle(EBUSY);
            return  (PX_ERROR);
        }

        if (uKeyboardInit((VOIDFUNCPTR)__consoleSioKbdPutRcvChar,
                          pConsoleSioChan) != ERROR_NONE) {             /*  USB 键盘                    */
            pcKeyboardDeinit();
            _ErrorHandle(EBUSY);
            return  (PX_ERROR);
        }
#endif
        break;

    case SIO_HUP:                                                       /*  关闭 SIO                    */
#if 0
        pcKeyboardDeinit();
#endif
        break;

    case SIO_BAUD_SET:                                                  /*  设置波特率                  */
        pConsoleSioChan->SIOCHAN_iBaud = (INT)lArg;                     /*  记录波特率                  */
        break;

    case SIO_BAUD_GET:                                                  /*  获得波特率                  */
        *((LONG *)lArg) = pConsoleSioChan->SIOCHAN_iBaud;
        break;

    case SIO_HW_OPTS_SET:                                               /*  设置硬件参数                */
        pConsoleSioChan->SIOCHAN_iHwOption = (INT)lArg;                 /*  记录硬件参数                */
        break;

    case SIO_HW_OPTS_GET:                                               /*  获取硬件参数                */
        *((LONG *)lArg) = pConsoleSioChan->SIOCHAN_iHwOption;
        break;

    default:
        _ErrorHandle(ENOSYS);
        return  (PX_ERROR);
    }

    return  (ERROR_NONE);
}
/*********************************************************************************************************
  console SIO 驱动程序
*********************************************************************************************************/
static SIO_DRV_FUNCS    _G_SioDrvFuncs = {
    (INT (*)(SIO_CHAN *,INT, PVOID))__consoleSioIoctl,
    __consoleSioStartup,
    __consoleSioCbInstall,
    __consoleSioPollRxChar,
    __consoleSioPollTxChar
};
/*********************************************************************************************************
** 函数名称: pcConsoleSioChanCreate
** 功能描述: 创建一个 SIO 通道
** 输　入  : uiChannel                 UART 通道号
** 输　出  : SIO 通道
** 全局变量:
** 调用模块:
*********************************************************************************************************/
SIO_CHAN  *pcFbConsoleSioChanCreate (VOID)
{
    __PCONSOLE_SIOCHAN  pConsoleSioChan;

    pConsoleSioChan = &_G_consoleSioChannel;
    pConsoleSioChan->SIOCHAN_sioChan.pDrvFuncs = &_G_SioDrvFuncs;       /*  SIO FUNC                    */

    return  (&pConsoleSioChan->SIOCHAN_sioChan);
}
/*********************************************************************************************************
  END
*********************************************************************************************************/
