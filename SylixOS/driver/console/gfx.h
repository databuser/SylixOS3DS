#pragma once

/*
 *   This file is part of fastboot 3DS
 *   Copyright (C) 2017 derrek, profi200
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "linux/types.h"


#define SCREEN_TOP          (0u)
#define SCREEN_BOT          (1u)

#define SCREEN_WIDTH_TOP    (400u)
#define SCREEN_HEIGHT_TOP   (240u)
#define SCREEN_SIZE_TOP     (SCREEN_WIDTH_TOP * SCREEN_HEIGHT_TOP * 2)
#define SCREEN_WIDTH_BOT    (320u)
#define SCREEN_HEIGHT_BOT   (240u)
#define SCREEN_SIZE_BOT     (SCREEN_WIDTH_BOT * SCREEN_HEIGHT_BOT * 2)

// TODO:
// Because we are using a VRAM allocator this may break any time.
#define VRAM_BASE           (0x18000000)
#define FRAMEBUF_TOP_A_1    ((void*)VRAM_BASE)
#define FRAMEBUF_BOT_A_1    (FRAMEBUF_TOP_A_1 + SCREEN_SIZE_TOP)
#define FRAMEBUF_TOP_A_2    (FRAMEBUF_BOT_A_1 + SCREEN_SIZE_BOT + SCREEN_SIZE_TOP) // Skip B1
#define FRAMEBUF_BOT_A_2    (FRAMEBUF_TOP_A_2 + SCREEN_SIZE_TOP)

#define RENDERBUF_TOP       ((void*)VRAM_BASE)
#define RENDERBUF_BOT       ((void*)VRAM_BASE + SCREEN_SIZE_TOP)

#define DEFAULT_BRIGHTNESS  (0x30)

/// Converts packed RGB8 to packed RGB565.
#define RGB8_to_565(r,g,b)  (((b)>>3)&0x1f)|((((g)>>2)&0x3f)<<5)|((((r)>>3)&0x1f)<<11)


/// Framebuffer format.
typedef enum
{
	GFX_RGBA8  = 0,  ///< RGBA8. (4 bytes)
	GFX_BGR8   = 1,  ///< BGR8. (3 bytes)
	GFX_RGB565 = 2,  ///< RGB565. (2 bytes)
	GFX_RGB5A1 = 3,  ///< RGB5A1. (2 bytes)
	GFX_RGBA4  = 4   ///< RGBA4. (2 bytes)
} GfxFbFmt;
