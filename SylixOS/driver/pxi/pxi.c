/*
 *   This file is part of fastboot 3DS
 *   Copyright (C) 2017 derrek, profi200
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define  __SYLIXOS_KERNEL
#include <SylixOS.h>

#include "types.h"
#include "arm11/hardware/pxi.h"
#include "arm11/hardware/interrupt.h"
#include "ipc_handler.h"

static vu32 g_lastResp[2] = {0};

static irqreturn_t pxiIrqHandler(void);

static inline void pxiSendWord(u32 word)
{
	while(REG_PXI_CNT & PXI_CNT_SFIFO_FULL);
	REG_PXI_SFIFO = word;
}

static inline u32 pxiRecvWord(void)
{
	while(REG_PXI_CNT & PXI_CNT_RFIFO_EMPTY);
	return REG_PXI_RFIFO;
}

static inline bool pxiFifoError(void)
{
	return (REG_PXI_CNT & PXI_CNT_FIFO_ERROR) != 0;
}

//手动触发arm9核产生中断
static inline void pxiSyncRequest(void)
{
	REG_PXI_SYNC_IRQ |= PXI_SYNC_IRQ_IRQ;
}

void PXI_init(void)
{
	REG_PXI_SYNC = PXI_SYNC_IRQ_ENABLE;
	REG_PXI_CNT = PXI_CNT_ENABLE_SRFIFO | PXI_CNT_FIFO_ERROR | PXI_CNT_FLUSH_SFIFO | PXI_CNT_SFIFO_NOT_FULL_IRQ_ENABLE;

    while(pxiRecvWord() != 0x99);
    pxiSendWord(0x11);

    API_InterVectorConnect(IRQ_PXI_SYNC,
                           (PINT_SVR_ROUTINE)pxiIrqHandler,
                           LW_NULL,
                           "pxi");
    API_InterVectorEnable(IRQ_PXI_SYNC);
    API_InterVectorEnable(IRQ_PXI_NOT_FULL);
}

static irqreturn_t pxiIrqHandler(void)
{
	u32 cmdCode = pxiRecvWord();

	if(cmdCode & IPC_CMD_RESP_FLAG)
	{
		g_lastResp[0] = cmdCode;
		g_lastResp[1] = pxiRecvWord();
	}

	return  (LW_IRQ_HANDLED);
}

u32 PXI_sendCmd(u32 cmd, const u32 *buf, u32 words)
{
	if (words > IPC_MAX_PARAMS) {
	    printk("pxi send data too large.\r\n");
	    return 1;
	}


	const u32 inBufs = IPC_CMD_IN_BUFS_MASK(cmd);
	const u32 outBufs = IPC_CMD_OUT_BUFS_MASK(cmd);
	for(u32 i = 0; i < inBufs; i++)
	{
		const IpcBuffer *const inBuf = (IpcBuffer*)&buf[i * sizeof(IpcBuffer) / 4];
		if(inBuf->ptr && inBuf->size) API_CacheFlush(DATA_CACHE, inBuf->ptr, inBuf->size);
	}
	for(u32 i = inBufs; i < inBufs + outBufs; i++)
	{
		const IpcBuffer *const outBuf = (IpcBuffer*)&buf[i * sizeof(IpcBuffer) / 4];
		if(outBuf->ptr && outBuf->size) API_CacheInvalidate(DATA_CACHE, outBuf->ptr, outBuf->size);
	}

	pxiSendWord(cmd);
	pxiSyncRequest();

	for(u32 i = 0; i < words; i++) pxiSendWord(buf[i]);
	if(pxiFifoError()) printk("pxi fifo error.\r\n");

	while(g_lastResp[0] != (IPC_CMD_RESP_FLAG | cmd));
	g_lastResp[0] = 0;
	const u32 res = g_lastResp[1];

#ifdef ARM111
	// The CPU may do speculative prefetches of data after the first invalidation
	// so we need to do it again. Not sure if this is a ARMv6+ thing.
	for(u32 i = inBufs; i < inBufs + outBufs; i++)
	{
		const IpcBuffer *const outBuf = (IpcBuffer*)&buf[i * sizeof(IpcBuffer) / 4];
		if(outBuf->ptr && outBuf->size) invalidateDCacheRange(outBuf->ptr, outBuf->size);
	}
#endif

	return res;
}

void PXI_sendPanicCmd(u32 cmd)
{
	pxiSendWord(cmd);
	pxiSyncRequest();
	while(pxiRecvWord() != (IPC_CMD_RESP_FLAG | cmd));
	// We don't care about the result
}
