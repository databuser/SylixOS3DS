#define  __SYLIXOS_KERNEL
#include <SylixOS.h>
#include <module.h>
#include <stdlib.h>
#include <string.h>

#include "arm11/hardware/pxi.h"
#include "ipc_handler.h"

typedef enum
{
    FS_DRIVE_SDMC = 0,
    FS_DRIVE_TWLN = 1,
    FS_DRIVE_TWLP = 2,
    FS_DRIVE_NAND = 3
} FsDrive;

/*
 *  BSP内存需求：
 *  1.虚拟磁盘由于是在内核用malloc申请的一块内存，所以需要保证BSP中内核data区域要大于这个磁盘内存的大小
 *  2.如果需要DCATTR_stMemSize磁盘缓存cache，这个cache是内核在内核堆上申请的，所以BSP中的data区也要大于这大小
 *  3.读猝发缓冲区大小为：猝发扇区数 * 扇区大小
 *  4.写猝发缓冲区大小为：猝发扇区数 * 扇区大小 * DCATTR_iMsgCount
 *  5.3和4如果使用cache一致性，那么内核申请的区域是DMA区域，负责是内核堆区
 *
 *  对于本例子而言：
 *  data区域至少需要：50MB + 8MB = 58MB
 *  DMA区域至少需要：(8192 * 512) + (8192 * 512 * 4) = 4MB + 16MB = 20MB
 *
 */

#define SECTOR_BYTES    512
#define SATA_DISK_SIZE  15360ll * LW_CFG_MB_SIZE

#define BUSTOR_SECTOR   64
#define DISK_CACHE_SIZE 64 * LW_CFG_KB_SIZE

LW_BLK_DEV  SATADEV_tBlkDev;
PVOID       SATADEV_pvOemdisk;

static INT sataBlkRd (PLW_BLK_DEV pDev, PVOID pvBuf, INT iStartBlk, INT iBlkNum)
{
    u32 cmdBuf[3];
    cmdBuf[0] = (u32)pvBuf;
    cmdBuf[1] = iBlkNum * SECTOR_BYTES;
    cmdBuf[2] = iStartBlk;

    PXI_sendCmd(IPC_CMD9_SD_READ, cmdBuf, 3);

    return  (ERROR_NONE);
}

static INT sataBlkWrt (PLW_BLK_DEV pDev, PVOID pvBuf, INT iStartBlk, INT iBlkNum)
{
    u32 cmdBuf[3];
    cmdBuf[0] = (u32)pvBuf;
    cmdBuf[1] = iBlkNum * SECTOR_BYTES;
    cmdBuf[2] = iStartBlk;

    PXI_sendCmd(IPC_CMD9_SD_WRITE, cmdBuf, 3);

    return  (ERROR_NONE);
}

static INT  sataBlkIoctl (PLW_BLK_DEV   pDev, INT  iCmd, LONG  lArg)
{
    switch (iCmd) {

    /*
     *  必须要支持的命令
     */
    case FIOSYNC:
    case FIODATASYNC:
    case FIOSYNCMETA:
    case FIOFLUSH:                                                      /*  将缓存写入磁盘              */

    case FIOUNMOUNT:                                                    /*  卸载卷                      */
    case FIODISKINIT:                                                   /*  初始化磁盘                  */
    case FIODISKCHANGE:                                                 /*  磁盘媒质发生变化            */
        break;

    case FIOTRIM:                                                            /* AHCI_TRIM_EN                 */
        break;
    /*
     *  低级格式化
     */
    case FIODISKFORMAT:                                                 /*  格式化卷                    */
        return  (PX_ERROR);                                             /*  不支持低级格式化            */

    /*
     *  FatFs 扩展命令
     */
    case LW_BLKD_CTRL_POWER:
    case LW_BLKD_CTRL_LOCK:
    case LW_BLKD_CTRL_EJECT:
        break;

    case LW_BLKD_GET_SECSIZE:
    case LW_BLKD_GET_BLKSIZE:
        *((LONG *)lArg) = pDev->BLKD_ulBytesPerSector;
        break;

    case LW_BLKD_GET_SECNUM:
        *((ULONG *)lArg) = (ULONG)pDev->BLKD_ulNSector;
        break;

    case FIOWTIMEOUT:
    case FIORTIMEOUT:
        break;

    default:
        _ErrorHandle(ENOSYS);
        return  (PX_ERROR);
    }

    return  (ERROR_NONE);
}

static INT  sataBlkReset (PLW_BLK_DEV  pDev)
{
    return  (ERROR_NONE);
}

static INT  sataBlkStatusChk (PLW_BLK_DEV  pDev)
{
    return  (ERROR_NONE);
}

static s32 fMount(FsDrive drive)
{
    const u32 cmdBuf = drive;
    return PXI_sendCmd(IPC_CMD9_FMOUNT, &cmdBuf, 1);
}

int sd_init (void)
{
    PLW_BLK_DEV  pBlkDev = &SATADEV_tBlkDev;
    LW_DISKCACHE_ATTR dcattrl;

    /*
    *  配置块设备参数
    */
    pBlkDev->BLKD_pcName            = "/satac0d0";
    pBlkDev->BLKD_ulNSector         = SATA_DISK_SIZE / SECTOR_BYTES;
    pBlkDev->BLKD_ulBytesPerSector  = SECTOR_BYTES;
    pBlkDev->BLKD_ulBytesPerBlock   = SECTOR_BYTES;
    pBlkDev->BLKD_bRemovable        = LW_FALSE;
    pBlkDev->BLKD_iRetry            = 1;
    pBlkDev->BLKD_iFlag             = O_RDWR;
    pBlkDev->BLKD_bDiskChange       = LW_TRUE;
    pBlkDev->BLKD_pfuncBlkRd        = sataBlkRd;
    pBlkDev->BLKD_pfuncBlkWrt       = sataBlkWrt;
    pBlkDev->BLKD_pfuncBlkIoctl     = sataBlkIoctl;
    pBlkDev->BLKD_pfuncBlkReset     = sataBlkReset;
    pBlkDev->BLKD_pfuncBlkStatusChk = sataBlkStatusChk;

    pBlkDev->BLKD_iLogic            = 0;
    pBlkDev->BLKD_uiLinkCounter     = 0;
    pBlkDev->BLKD_pvLink            = LW_NULL;

    pBlkDev->BLKD_uiPowerCounter    = 0;
    pBlkDev->BLKD_uiInitCounter     = 0;

    dcattrl.DCATTR_pvCacheMem       = LW_NULL;
    dcattrl.DCATTR_stMemSize        = (size_t)(DISK_CACHE_SIZE);
//    dcattrl.DCATTR_iBurstOpt        = LW_DCATTR_BOPT_CACHE_COHERENCE;
    dcattrl.DCATTR_iBurstOpt        = 0;
    dcattrl.DCATTR_iMaxRBurstSector = (INT)BUSTOR_SECTOR;
    dcattrl.DCATTR_iMaxWBurstSector = (INT)BUSTOR_SECTOR;
    dcattrl.DCATTR_iMsgCount        = 4;
    dcattrl.DCATTR_bParallel        = LW_TRUE;
    dcattrl.DCATTR_iPipeline        = 1;

    PXI_init();

    fMount(FS_DRIVE_SDMC);

    SATADEV_pvOemdisk = (PVOID)API_OemDiskMount2("/media/sdcard",
                                                 pBlkDev,
                                                 &dcattrl);
    if (!SATADEV_pvOemdisk) {
        printk("oem disk mount failed\r\n");
    }

    return 0;
}
