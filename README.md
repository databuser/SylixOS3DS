### 1. Nintendo 3DS简介
Nintendo 3DS是任天堂2011年发布的掌机设备，可以实现裸眼3D功能。

![Nintendo3DSPic](SylixOS/doc/picture/Nintendo3DSPic.jpg)

### 2. SylixOS3DS简介
SylixOS3DS是将SylixOS移植到了Nintendo 3DS平台，目前仅支持显示。

![SylixOS3DSPic](SylixOS/doc/picture/SylixOS3DSPic.png)

### 3. 编译
- 编译需要RealEvo-IDE环境，可以到翼辉信息官网申请体验版。[申请链接](https://www.acoinfo.com/html/experience.php)
- 创建base工程，编译器选择arm，架构选择arm1176jzf-s平台。
- 导入bsp工程，关联base，编译生成bsp_3ds_a11.bin内核镜像。

### 4. 运行SylixOS
- 将SD卡格式化为FAT分区。
- 需要将bsp_3ds_a11.bin打包成sylixos.firm格式才能被引导。
- 将sylixos.firm复制到SD卡下，插入到设备上启动。
- 3DS设备需要使用fastboot3DS来引导sylixos.firm。

